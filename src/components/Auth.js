import React from 'react'
import {Form,Button} from 'react-bootstrap'
import login from './ProtectedLogin'
import './style.css'
  
export default function Auth(props) {
    return (
      <div className="frm-login">
            <Form>
                <Form.Group controlId="formBasicEmail">
                    <Form.Label>Email address</Form.Label>
                    <Form.Control type="email" placeholder="Enter email" />
                </Form.Group>

                <Form.Group controlId="formBasicPassword">
                    <Form.Label>Password</Form.Label>
                    <Form.Control type="password" placeholder="Password" />
                </Form.Group>
                <Button variant="primary"
                    onClick={()=>{
                        login.login(()=>{
                            props.history.push("/welcome")
                        })
                    }}
                >Login</Button>
            </Form>
      </div>
    )
}
