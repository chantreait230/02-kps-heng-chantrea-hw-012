import React from 'react'
import {Route,Redirect} from "react-router-dom";
import login from './ProtectedLogin'

export const ProtectedRoute=({component:Component, ...rest})=>{
    return (
        <Route {...rest} render={
            (props)=>{
                if(login.isLogin()){
                    return <Component {...props}/>
                }else{
                    return <Redirect to={
                        {
                            pathname:"/auth",
                            state:{
                                from: props.location
                            }
                        }
                    } />
                }
                
            }
        }/>
    )
}
