import React from 'react'
import {Carousel} from 'react-bootstrap'
export default function Slide() {
    return (
            <Carousel>
                <Carousel.Item style={{height:'750px',overflow:'hidden'}}>
                    <img
                    className="d-block w-100"
                    src="https://iflix-images.akamaized.net/5d1ac686e4b0d897c6177a0a_l_carousel-landscape-wide?transform=true&v=1&resize[0]=2400"
                    alt="First slide"
                    />
                    <Carousel.Caption>
                    <h3>First slide label</h3>
                    <p>Nulla vitae elit libero, a pharetra augue mollis interdum.</p>
                    </Carousel.Caption>
                </Carousel.Item>
                <Carousel.Item style={{height:'750px',overflow:'hidden'}}>
                    <img
                    className="d-block w-100"
                    src="https://iflix-images.akamaized.net/5d1ac686e4b0d897c6177a07_l_carousel-landscape-wide?transform=true&v=1&resize[0]=2400"
                    alt="Third slide"
                    />

                    <Carousel.Caption>
                    <h3>Second slide label</h3>
                    <p>Lorem ipsum dolor sit amet, consectetur adipiscing elit.</p>
                    </Carousel.Caption>
                </Carousel.Item>
                <Carousel.Item style={{height:'750px',overflow:'hidden'}}>
                    <img
                    className="d-block w-100"
                    src="https://iflix-images.akamaized.net/5d2d93c1e4b057c85dc75ba2_l_carousel-landscape-wide?transform=true&v=1&resize[0]=2400"
                    alt="Third slide"
                    />

                    <Carousel.Caption>
                    <h3>Third slide label</h3>
                    <p>Praesent commodo cursus magna, vel scelerisque nisl consectetur.</p>
                    </Carousel.Caption>
                </Carousel.Item>
            </Carousel>
    )
}
