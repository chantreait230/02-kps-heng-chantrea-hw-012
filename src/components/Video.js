import React from 'react'
import {Link,Route} from "react-router-dom";
import './style.css'
import {Navbar,Nav} from 'react-bootstrap'

const video=[
    {
      id:'animation',
      name:'Animation',
      resources:[
        {
           id:'action',
           name:'Action'
        },
        {
         id:'romance',
         name:'Romance'
       },
       {
         id:'company',
         name:'Company'
       }
      ]
   },
   {
     id:'movie',
     name:'Movie',
     resources:[
       {
          id:'adventure',
          name:'Adventure'
       },
       {
        id:'company',
        name:'Company'
      },
      {
        id:'crime',
        name:'Crime'
      },
      {
        id:'documentary',
        name:'Documentary'
      }
     ]
  }
 ]
 let listMovie=undefined;

 function Resource ({ match }) {
    const topic = video.find(({ id }) => id === match.params.videoId)
    .resources.find(({ id }) => id === match.params.subId)
    return (
      <div className="resource">
        <h3>{topic.name}</h3>
      </div>
    )
  }
  
  function MyVideo ({ match }) {
    listMovie = video.find(({ id }) => id === match.params.videoId)
    return (
      <div className="sub-menu">
        <Navbar collapseOnSelect expand="lg" variant="light" style={{fontFamily:"'Roboto', sans-serif"}}>
                <Navbar.Toggle aria-controls="responsive-navbar-nav" />
                <Navbar.Collapse id="responsive-navbar-nav">
                    <Nav className="mr-auto">
                      {listMovie.resources.map((sub) => (
                        <Nav.Link key={sub.id} as={Link} to={`${match.url}/${sub.id}`}>{sub.name}</Nav.Link>
                      ))}
                    </Nav> 
                </Navbar.Collapse>
        </Navbar>
        <Route path={`${match.path}/:subId`} component={Resource} />
      </div>
    )
  }
  
  function VideoList ({ match }) {
    return (
      <div className="left-menu">
        <ul>
          {video.map(({ name, id }) => (
            <li key={id}>
              <Nav.Link key={id} as={Link} to={`${match.url}/${id}`}>{name}</Nav.Link>
            </li>
          ))}
        </ul>
        <Route path={`${match.path}/:videoId`} component={MyVideo}/>
      </div>
    )
  }
export default function Video(props) {
    return (
        <div className="container">
            <Route path='/video' component={VideoList} />
        {/* <h3>{listMovie===undefined ? "Please select topic":""}</h3> */}
    
        </div>
    )
}
