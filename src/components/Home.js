
import React from 'react'
import Slide from './Slide'
import MyCard from './MyCard'

export default function Home(props) {
    return (
        <div>
            <Slide/>
            <MyCard movie={props.toHome}/>
        </div>
    )
}


