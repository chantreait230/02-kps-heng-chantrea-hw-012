class protectedLogin{
    constructor(){
        this.pro_login=false;
    }
    login(cb){
        this.pro_login=true
        cb()
    }
    isLogin(){
        return this.pro_login
    }
}
export default new protectedLogin()