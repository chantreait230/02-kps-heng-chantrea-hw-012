import React from 'react'
import {Link} from "react-router-dom";
import queryString from 'query-string'
import './style.css'
import {Nav,Modal,Button} from 'react-bootstrap'

function Message(props) {
  const [show, setShow] = React.useState(false);

  const handleClose = () => setShow(false);
  const handleShow = () => setShow(true);

  return (
    <>
      <Nav.Link onClick={handleShow} key={props.data.id} as={Link} to={`/account?name=${props.data.name}`}>{props.data.name}</Nav.Link>

      <Modal show={show} onHide={handleClose}>
        <Modal.Header closeButton>
        <Modal.Title>{props.data.name}</Modal.Title>
        </Modal.Header>
        <Modal.Body>Hello {props.data.name}</Modal.Body>
        <Modal.Footer>
          <Button variant="secondary" onClick={handleClose}>
            Close
          </Button>
        </Modal.Footer>
      </Modal>
    </>
  );
}

const accounts=[
    {
      id:1,
      name:'netflix'
    },
    {
     id:2,
     name:'Zillow Group'
   },
   {
     id:3,
     name:'Yahoo'
   },
   {
     id:4,
     name:'Modus Create'
   }
  ]
export default function Account(props) {
    let getName=queryString.parse(props.location.search)
    let account=accounts.map((item)=>
        <li key={item.id}>
            <Message data={item}/>
        </li>
    )
    return (
        <div className="container">
          <div className="account">
            <ul>
              {account}
            </ul>
          </div>
          <div style={{textAlign:'center',padding:'10px 0px',color:'red'}}>{getName.name===undefined ? "There are no name":` The name in query string is "${getName.name}"`}</div>
            
        </div>
    )
}
