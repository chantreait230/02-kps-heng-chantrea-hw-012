import React from 'react'
import {Card,Button} from 'react-bootstrap'
import {Link} from "react-router-dom";

export default function MyCard(props) {
    const card=props.movie.map((item)=>
        <Card key={item.id} style={{ width: '12rem',display:'inline-block',margin:'8px' }}>
            <Card.Img variant="top" src={item.img} />
            <Card.Body>
                <Card.Title style={{fontFamily:"'Hanuman', serif"}}>{item.title}</Card.Title>
                <Link to={`/post/${item.id}`}><Button variant="danger">More</Button></Link>
            </Card.Body>
        </Card>
    )
    return (
        <div style={{marginLeft:'20px'}}>
            <div style={{padding:'20px',fontSize:'29px',fontFamily:"'Roboto', sans-serif"}}>Popular</div>
            {card}
        </div>
    )
}
