import React from 'react'
import {Navbar,Nav} from 'react-bootstrap'
import {Link} from "react-router-dom";

export default function Menu() {
    return (
        <div className="container-fluid bg-light p-2 ">
            <Navbar collapseOnSelect expand="lg" variant="light" style={{fontFamily:"'Roboto', sans-serif"}}>
                <Navbar.Brand as={Link} to="/" >MyMoViE</Navbar.Brand>
                <Navbar.Toggle aria-controls="responsive-navbar-nav" />
                <Navbar.Collapse id="responsive-navbar-nav">
                    <Nav className="mr-auto">
                    <Nav.Link as={Link} to="/home">HOME</Nav.Link>
                    <Nav.Link as={Link} to="/video">VIDEO</Nav.Link>
                    <Nav.Link as={Link} to="/account">ACCOUNT</Nav.Link>
                    <Nav.Link as={Link} to="/auth">AUTH</Nav.Link>
                    </Nav>
                    <Nav>
                    <Nav.Link href="#deets">SIGN IN</Nav.Link>
                    </Nav>
                </Navbar.Collapse>
            </Navbar>
        </div>
    )
}
