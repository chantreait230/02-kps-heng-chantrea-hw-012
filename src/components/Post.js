import React from 'react'
import './style.css'
import {Card} from 'react-bootstrap'

export default function Post({match,toPost}) {
    var dataPost=toPost.find((item)=> item.id==match.params.id);
    return (
        <div className="container-fluid box-video">
            <div className="container" style={{padding:'20px 0px'}}>
                <Card key={dataPost.id} style={{ width: '21rem',margin:'auto',border:'none'}}>
                    <Card.Img variant="top" src={dataPost.img} />
                </Card>
                
            </div>
            <div className="container">
                <div className="row">
                    <div className="col-xl-4 col-lg-4 col-md-4 detail">
                        <div style={{fontFamily:"'Hanuman', serif",fontSize:'22px',padding:'10px 0px'}}>ID : {dataPost.id}</div>
                        <div style={{fontFamily:"'Hanuman', serif",fontSize:'22px',padding:'10px 0px'}}>Title : {dataPost.title}</div>
                        <div style={{fontFamily:"'Hanuman', serif",fontSize:'22px',padding:'10px 0px'}}>Time : {dataPost.minut}</div>
                    </div>
                    <div className="col-xl-8 col-lg-8 col-md-8">
                        <h3 style={{padding:'120px 0px'}}>This is content from post {dataPost.id}</h3>
                    </div>
                </div>
            </div>
            
        </div>
    )
}
