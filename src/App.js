import React, { Component } from 'react'
import 'bootstrap/dist/css/bootstrap.min.css';
import {
  BrowserRouter as Router,
  Switch,
  Route
} from "react-router-dom";
import Menu from './components/Menu'
import Home from './components/Home'
import Video from './components/Video'
import Main from './components/Main'
import Post from './components/Post'
import Account from './components/Account'
import Auth from './components/Auth'
import Welcome from './components/Welcome'
import {ProtectedRoute} from './components/ProtectedRoute'

export default class App extends Component {
  constructor(props) {
    super(props)

    this.state = {
         movie:[
             {
                 id:1,
                 title:"ទាសករស្នេហ៍",
                 img:"https://iflix-images.akamaized.net/5cb6c875e4b0bda0b4250292-1555482741990_s_300x450?transform=true&v=1&resize[0]=208&resize[1]=312",
                 minut:'1:20 nm'
             },
             {
                id:2,
                title:"41 ថ្ងៃចុងក្រោយ",
                img:"https://iflix-images.akamaized.net/5bcd70cde4b0d897abb5c37e-1540190414495_s_300x450?transform=true&v=1&resize[0]=208&resize[1]=312",
                minut:'1:45 nm'
            },
            {
                id:3,
                title:"ខ្មោចនាងនាថ",
                img:"https://iflix-images.akamaized.net/5d2fec17e4b057c85dc7c18c-1563421721325_s_300x450?transform=true&v=1&resize[0]=208&resize[1]=312",
                minut:'1:23 nm'
            },
            {
                id:4,
                title:"ដប់មុឺន",
                img:"https://iflix-images.akamaized.net/5cd39baae4b006f4afbe05f1-1557371819831_s_300x450?transform=true&v=1&resize[0]=208&resize[1]=312",
                minut:'1:20 nm'
            }
            ,
            {
                id:5,
                title:"Sosor Konlong",
                img:"https://iflix-images.akamaized.net/5d2feba9e4b057c85dc7c18b-1563421610731_s_300x450?transform=true&v=1&resize[0]=208&resize[1]=312",
                minut:'1:15 nm'
            }
            ,
            {
                id:6,
                title:"Love Arumirai",
                img:"https://iflix-images.akamaized.net/5c1c5e1ce4b0d897abc2a7dd-1545362973559_s_300x450?transform=true&v=1&resize[0]=208&resize[1]=312",
                minut:'1:58 nm'
            },
            {
                id:7,
                title:"The Witch",
                img:"https://iflix-images.akamaized.net/5d562b00e4b078ceb8dabf4d-1565928209281_s_300x450?transform=true&v=1&resize[0]=208&resize[1]=312",
                minut:'1:30 nm'
            },
            {
                id:8,
                title:"Mind Memory",
                img:"https://iflix-images.akamaized.net/5c1c5ec3e4b0a5c41069c96c-1545363140404_s_300x450?transform=true&v=1&resize[0]=208&resize[1]=312",
                minut:'1:25 nm'
            }
            ,
            {
                id:9,
                title:"Wolf Warrior II",
                img:"https://iflix-images.akamaized.net/5c4687dde4b0a5c41078694e-1562051237344_s_300x450?transform=true&v=1&resize[0]=208&resize[1]=312",
                minut:'1:35 nm'
            }
         ]
    }
}

  render() {
    return (
      <div>
        <Router>
          <Menu/>
          <Switch>
            <Route path="/" exact component={Main}></Route>
            <Route path="/home" render={()=><Home toHome={this.state.movie}/>}></Route>
            <Route path="/post/:id" render={(props)=><Post {...props} toPost={this.state.movie}/>}></Route>
            <Route path="/video"  component={Video}></Route>
            
            <Route path="/account" component={Account}></Route>
            <Route path="/auth" component={Auth}></Route>
            <ProtectedRoute path="/welcome" component={Welcome}/>
          </Switch>
        </Router>
      </div>
    )
  }
}

